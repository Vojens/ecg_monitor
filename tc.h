#ifndef tc
#define tc

void TC0_interrupt_on(void);

void TC0_interrupt_off(void);

void TC0_prescaler_selector(uint16_t ps);

void TC0_set_reg(uint8_t reg);

uint8_t TC0_get_reg(void);

void TC1_interrupt_on(void);

void TC1_interrupt_off(void);

void TC1_prescaler_selector(uint16_t ps);

void TC1_set_reg(uint8_t reg);

uint16_t TC1_get_reg(void);


#endif
