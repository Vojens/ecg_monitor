/***************************************************************************************
* FILE NAME: lcd.c                                                                   
* AUTHOR: ALI AL HUBAIL																 
* PROVIDE:																			 
* THIS FILE HAS FUNCTIONS TO INISIALIZE HD44780U (LCD-II), TO PRINT A CHARACTER,
* TO CLEAR THE SCREEN, TO GO BACK TO UPPER LEFT LOCATION, AND TO GO TO SPESIFIC LOCATION. 
****************************************************************************************/

#include <avr\io.h>
#include <inttypes.h>
#include <avr\interrupt.h>



#ifndef LCD
#define LCD
	void LCD2ms(void);
	void LCD5ms(void);
	void LCDinit(void);
	void LCDchar(const char data);
	void LCDclear(void);
	void LCDhome(void);
	void LCDgotoxy(const uint8_t row, const uint8_t column);
	void LCDstring(const char theString[]);
	void LCDhex(const uint8_t theNumber);
	void LCDuint8(const uint8_t theNumber);
#endif
