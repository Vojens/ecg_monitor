/***************************************************************************************
* FILE NAME: lcd.c                                                                   
* AUTHOR: ALI AL HUBAIL																 
* PROVIDE:																			 
* THIS FILE HAS FUNCTIONS TO INISIALIZE HD44780U (LCD-II), TO PRINT A CHARACTER,
* TO CLEAR THE SCREEN, TO GO BACK TO UPPER LEFT LOCATION, TO GO TO SPESIFIC LOCATION,
* TO PRINT A STRING, TO PRINT A HEX NUMBER, AND TO PRINT AN INTEGER
****************************************************************************************/

#include <avr\io.h>
#include <inttypes.h>
#include <avr\interrupt.h>

#include "lcd.h"


void LCDchar(const char theCharacter)
{
	
	PORTC = PINC | 0b00000100;	//RS = 1
	
	PORTC = PINC | 0b00000001;	// E = 1

	PORTC =(PINC & 0x0F)|(theCharacter & 0xF0);	// send upper nibble

	PORTC = PINC & 0b11111110; // E = 0

	//LCD2ms();

	PORTC = PINC | 0b00000001;	// E = 1

	PORTC=(PINC &0x0F)|((theCharacter<<4)&0xF0);// send lower nibble

	PORTC = PINC & 0b11111110; // E = 0

	PORTC = PINC & 0b11111011; //RS = 0
	LCD2ms();
}


void LCD2ms(void)
{

	uint16_t counter = 1700;
	while(counter > 0)
	{
		counter--;
	}

}

void LCD5ms(void)
{
	uint16_t counter = 4250;
	while(counter > 0)
	{
		counter--;
	}


}


void LCDinit(void)
{
	
	/*
	More that 15 ms is required to be waited by the manufacturer 
	till Vcc rises to 4.7, but , as noticed, this is not necessary,
	*/
	LCD5ms();
	LCD5ms();
	LCD5ms();
	LCD2ms();
	

	DDRC = DDRC | 0b11110000;
	PORTC = PINC & 0b11111000;
	DDRC = DDRC | 0b00000111;
	
		
	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b00110000);	// function set
	PORTC = PINC & 0b11111110; // E = 0
	LCD5ms();

	PORTC = PINC | 0b00000001;	// E = 1		
	PORTC =(PINC & 0x0F) | (0b00110000);	// function set
	PORTC = PINC & 0b11111110; // E = 0
	LCD2ms();
		
	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b00110000);	// function set
	PORTC = PINC & 0b11111110; // E = 0
	LCD2ms();// this one is needed. or the display is off

	
	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b00100000);	// sets to 4 bit operation - datasheet page 42
	PORTC = PINC & 0b11111110; // E = 0
	LCD2ms(); // this one is needed. or the display is off
	

	/*****************************************************************
	* Function Set: Datasheet Page 27-28
	* DB7 - DB0 : 0 0 1 DL N F * *
	* DL (data length) = 0 when 4 mode and 1 when 8 mode
	* N (Number of Display lines) = 0 when 1 line, and 1 when 2 lines
	* F (font) refer to table 8 on page 29
	* (*) = don't care
	******************************************************************/
	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b00100000);
	PORTC = PINC & 0b11111110; // E = 0
	//LCD2ms();

	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b10000000);
	PORTC = PINC & 0b11111110; // E = 0
	//LCD2ms();

	

	/*****************************************************************
	* Display On/ Off control: Datasheet Page 42
	* DB7 - DB0 : 0 0 0 0 1 D C B
	* D (Display) = 0
	* Sets entire display (D) on/off, cursor on/off (C), and
	* blinking of cursor position character (B).
	******************************************************************/

	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b00000000);
	PORTC = PINC & 0b11111110; // E = 0
	//LCD2ms();
	
	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b11000000);
	PORTC = PINC & 0b11111110; // E = 0
	//LCD2ms();
	

	/*****************************************************************
	* Entry Mode Set: Datasheet Page 27-28, 42
	* DB7 - DB0 : 0 0 0 0 0 1 I/D S
	* Sets mode to increment the address by one and to shift the
	* cursor to the right at the time of write to the DD/CGRAM.
	* Display is not shifted.
	* I/D: Increments (I/D = 1) or decrements (I/D = 0) the DDRAM address by 1 
	* when a character code is written into or read from DDRAM.
	* The cursor or blinking moves to the right when incremented by 1 
	* and to the left when decremented by 1. The same applies to writing and 
	* reading of CGRAM. 
	* S: Shifts the entire display either to the right (I/D = 0) or to 
	* the left (I/D = 1) when S is 1. The display does not shift if S is 0.
	* If S is 1, it will seem as if the cursor does not move but the display does. 
	* The display does not shift when reading from DDRAM. Also, writing into 
	* or reading out from CGRAM does not shift the display.
	******************************************************************/	


	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b00000000);
	PORTC = PINC & 0b11111110; // E = 0
	//LCD2ms();
	
	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b01100000);
	PORTC = PINC & 0b11111110; // E = 0
	//LCD2ms();



	//-----------
	//Display Clear
	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b00000000);
	PORTC = PINC & 0b11111110; // E = 0
	//LCD2ms();
	
	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b00010000);
	PORTC = PINC & 0b11111110; // E = 0
	//LCD2ms();
	//---------------



	
	//-----------------
	// Return Home
	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b00000000);
	PORTC = PINC & 0b11111110; // E = 0
	//LCD2ms();
	
	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b00100000);
	PORTC = PINC & 0b11111110; // E = 0
	//LCD2ms();	

	LCD5ms();

	LCDhome();

	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b00000000);	// without cursor
	PORTC = PINC & 0b11111110; // E = 0
	LCD2ms();

	PORTC = PINC | 0b00000001;	// E = 1
	PORTC =(PINC & 0x0F) | (0b11000000);	// without cursor
	PORTC = PINC & 0b11111110; // E = 0
	LCD2ms();
	
	LCDclear();

	

}

void LCDclear(void)
{

	PORTC = PINC | 0b00000001;	// E = 1

	PORTC =(PINC & 0x0F)|(0b00000000);

	PORTC = PINC & 0b11111110; // E = 0

	LCD2ms();

	
	PORTC = PINC | 0b00000001;	// E = 1

	PORTC =(PINC & 0x0F)|(0b00010000);

	PORTC = PINC & 0b11111110; // E = 0

	LCD2ms();
}

void LCDhome(void)
{

	PORTC = PINC | 0b00000001;	// E = 1

	PORTC =(PINC & 0x0F)|(0b00000000);

	PORTC = PINC & 0b11111110; // E = 0

	LCD2ms();

	
	PORTC = PINC | 0b00000001;	// E = 1

	PORTC =(PINC & 0x0F)|(0b00100000);

	PORTC = PINC & 0b11111110; // E = 0

	LCD2ms();
}

void LCDgotoxy(const uint8_t row, const uint8_t column)
{
	

		PORTC = PINC | 0b00000001;	// E = 1
		if(row == 0)
		{
			PORTC =(PINC & 0x0F) | (0b10000000);	// function set	
		}
		else	//if(row == 1)
		{
			PORTC =(PINC & 0x0F) | (0b11000000);	// function set	
		}
		
		PORTC = PINC & 0b11111110; // E = 0
		LCD2ms();



		PORTC = PINC | 0b00000001;	// E = 1
		PORTC =(PINC & 0x0F) | (column << 4);	// function set
		PORTC = PINC & 0b11111110; // E = 0
		
		LCD2ms();


}

void LCDstring(const char theString[])
{
	//for(char i = 0; theString[i] != 0 ; i++)
	char i = 0x00;
	while(theString[i] != 0x00)
	{
		LCDchar(theString[i]);
		i++;
	}

}

void LCDhex(const uint8_t theNumber)
{
	if(((theNumber & 0xf0) >> 4) <= 0x09)	//high nipple
	{
			LCDchar(((theNumber & 0xF0) >> 4) + 0x30);		
	}
	else
	{
			LCDchar(((theNumber & 0xf0) >> 4) + 0x37);
	}
	
	
	if((theNumber & 0x0f) <= 0x09)	//low nipple
	{
			LCDchar((theNumber & 0x0f) + 0x30);
	}
	else
	{
			LCDchar((theNumber & 0x0f) + 0x37);
	}

}


void LCDuint8(const uint8_t theNumber)
{
	uint8_t temp = theNumber;
	
	if(theNumber >= 100)
	{
	
		LCDchar((((theNumber - (temp % 100))/100) & 0x0F)+ 0x30);
		temp = temp % 100;
	}
	if(theNumber >= 10)
	{
	
	LCDchar((((temp - (temp % 10))/10) & 0x0F)+ 0x30);
	
	temp = temp % 10;
	}
	//LCDchar((((temp - (temp % 1))/1) & 0x0F)+ 0x30);
	LCDchar((temp & 0x0F)+ 0x30);
	
}


