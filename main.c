//PA0 = 1 when 60%
//PA1 = sonic-alert
//http://www.americanheart.org/presenter.jhtml?identifier=4736


#include <avr\io.h>
#include <inttypes.h>
#include <avr\interrupt.h>
#include <avr\sleep.h>

#include "lcd.h"
#include "ac.h"
#include "int.h"
#include "tc.h"


#define FOSC 16E6
#define BAUD 115200//96E2

volatile uint8_t isSionicAlertOn;
volatile uint8_t isCharPrinted;
volatile uint8_t counter;
volatile uint8_t isSonicAlertOn;
volatile uint8_t oldNoOfBeats;
volatile uint8_t newNoOfBeats;
volatile uint8_t maxNoOfBeats;
volatile uint8_t minNoOfBeats;
volatile uint8_t enableHeartBeat;


ISR (INT0_vect)//PD_2 // AC on or off
{
	if(AC_isOn())
	{
		AC_off();
		TC1_interrupt_off();
			
		if(isCharPrinted)
		{
			LCDgotoxy(1,15);
			LCDchar(' ');
		}
	}
	else
	{
		AC_on();
		TC1_set_reg(0);//
		TC1_interrupt_on();
		
	}

}


ISR (INT1_vect)//isSonicAlertOn
{
	
	isSonicAlertOn = isSonicAlertOn ^ 0xff;
}


ISR (ANA_COMP_vect)
{
		
	TC0_set_reg(105);
	counter = 0;
	
	PORTA = PORTA | 0b00000001;
	if(isSonicAlertOn)
	{
		PORTA = PORTA | 0b00000010;	
	}
	
	AC_off();
	TC0_interrupt_on();
}

ISR (TIMER0_OVF_vect)
{

	TC0_set_reg(105);
	
	if(counter > 1)
	{
		
		
		PORTA = PORTA & 0b11111110;
		PORTA = PORTA & 0b11111101;	//turn off sonic-alert 

		


		counter = 0;

		TC0_interrupt_off();
		
		if(enableHeartBeat)
		{
			LCDgotoxy(1,15);	
			if(isCharPrinted)
			{
				LCDchar(' ');
			}
			else
			{
				LCDchar(0b11110100);
			}
			isCharPrinted = isCharPrinted ^ 0xff;
		}
		
		
		oldNoOfBeats = newNoOfBeats;
		newNoOfBeats = 58593/(TC1_get_reg()/16);	//93500/16 = 58593
		TC1_set_reg(0);//

		/*
		if(newNoOfBeats != oldNoOfBeats)
		{
			LCDgotoxy(1,4);
			LCDuint8(newNoOfBeats);
			if(oldNoOfBeats >= 100)
			{
				LCDchar(' ');
			}

		}
		
		*/
		if(newNoOfBeats < minNoOfBeats)
		{
			/*
			LCDgotoxy(1,11);
			LCDuint8(newNoOfBeats);
			if(minNoOfBeats >= 100)
			{
				LCDchar(' ');
			}
			*/
			minNoOfBeats = newNoOfBeats;
		}

		else if(newNoOfBeats > maxNoOfBeats)
		{
			/*
			LCDgotoxy(0,13);
			LCDuint8(newNoOfBeats);
			*/
			maxNoOfBeats = newNoOfBeats;
		}
		


		AC_on();
	}

	counter++;
}





char compareString(const char *str1, const char *str2)
{
	
	char *temp1, *temp2;
	temp1 = str1;
	temp2 = str2;
	
	while((*temp1 != 0) && (*temp2 != 0))
	{
		//LCDchar(*temp1++);

		if(*temp1 != *temp2)
		{
			return 0x00;
		}

		*temp1++;
		*temp2++;
	}

	if(*temp1 != 0 || *temp2 != 0)
	{
		return 0x00;
	}
	else
	{
		return 0xff;
	}
/*
	while(*temp2 != 0)
	{
		LCDchar(*temp2++);
	}
*/
}

void shiftString(const char *str, const char character)
{
	char *temp1, *temp2;
	temp1 = str;
	temp2 = str;

	char character0 = *temp1;
	*temp1++;
	char character1 = *temp1;
	*temp1++;
	char character2 = *temp1++;

	*temp2++ = character1;
	*temp2++ = character2;
	*temp2 = character;
}

int main(void)
{

	SPH = 0x08;
	SPL = 0x3F;


	LCDinit();

	INT0_on();
	INT1_on();
	
	INT0_fallingEdgeInterrupt();
	INT1_fallingEdgeInterrupt();

	DDRA = 0xff;
	PORTA =0x00;

	PORTC = isSonicAlertOn;
	isSonicAlertOn = 0xff;

		
	ACinit();
	AC_useInterrupt();

	LCDstring("ECG MONITOR");


	isCharPrinted = 0x00;

	TC0_prescaler_selector(1024);

	TC0_interrupt_off();

	
	
	TC1_prescaler_selector(1024);

	TC1_interrupt_on();

	TC1_set_reg(0);//
	//TCNT1 = 0;

	maxNoOfBeats = 0x00;
	minNoOfBeats = 0xff;
	
	

	char USARTdata = 0;
	UBRRH = 0;
	UBRRL = 0b00001000;//( FOSC / (16*BAUD) ) - 1;
	
	UCSRC = 0b10101110;
	UCSRB = 0b00011000;
	


	LCDgotoxy(1,0);
	 
	 /*
	 char *a = "Hell";
	 char *b = "Hell";
	
	
	//comp(a,b);
	//LCDhex(a);
	//LCDhex(b);
	
	char max[4] = {'m','a', 'x','\0'};
	char min[4] = {'m','i', 'n','\0'};
	if(compareString(max,min))
	{
		LCDstring("equals");
	}
	else
	{
		LCDstring("not");
	}
	*/
	/*
	LCDchar(*a++);
	LCDchar(*a++);
	LCDchar(*a++);
	LCDchar(*b++);
	LCDchar(*b++);
	LCDchar(*b++);
	*/
	
	
	//LCDchar(*a++);
	//LCDchar(*a++);

//compareString(a, b);


/*
	char a[12]= "Cd";
	char b[12]= "Cd";

	if(compareString(&a, &b))
	{
		LCDgotoxy(1,0);
		LCDstring("equals");
	}
	else
	{
		LCDgotoxy(1,0);
		LCDstring("not");
	}

*/
	//char max[4] = {'m','a', 'x','\0'};
	//char min[4] = {'m','i', 'n','\0'};


	char *max = "max";
	char *min = "min";
	char *def = "def";
	
	
	
	char *input = "   ";
	/*
	char *temp1, *temp2;
	temp1 = a;
	temp2 = a;
	LCDgotoxy(1,0);
	LCDchar(*temp1++);
	LCDchar(*temp1++);
	LCDchar(*temp1++);
	shiftString(a,'f');
	LCDchar(*temp2++);
	LCDchar(*temp2++);
	LCDchar(*temp2++);

*/


	
	sei();	
	
	
	while(1)
	{
		if(UCSRA & 0x80)
		{
			USARTdata = UDR;

			//LCDgotoxy(1,0);
			//LCDhex(USARTdata);
			//LCDstring("ok");
			
						
			shiftString(input, USARTdata);
			if(compareString(input,max))
			{
				
				enableHeartBeat = 0;
						LCDgotoxy(1,0);
						LCDstring("Max beat: ");	
						LCDuint8(maxNoOfBeats);
						LCDchar(' ');
						if(isCharPrinted)
						{
							LCDgotoxy(1,15);
							LCDchar(' ');
						}
			}

			else if(compareString(input,min))
			{

				enableHeartBeat = 0;
				LCDgotoxy(1,0);
				LCDstring("Min beat: ");	
				LCDuint8(minNoOfBeats);
				LCDchar(' ');
				if(isCharPrinted)
				{
					LCDgotoxy(1,15);
					LCDchar(' ');
				}

				uint8_t temp = minNoOfBeats;
	
				if(minNoOfBeats >= 100)
				{
	
					UDR = ((((minNoOfBeats - (temp % 100))/100) & 0x0F)+ 0x30);
					temp = temp % 100;
					LCD5ms();
					LCD5ms();
					LCD5ms();
					LCD5ms();
				}
				if(minNoOfBeats >= 10)
				{
	
					UDR = ((((temp - (temp % 10))/10) & 0x0F)+ 0x30);
	
				temp = temp % 10;
				LCD5ms();
					LCD5ms();
					LCD5ms();
					LCD5ms();
				}
				//LCDchar((((temp - (temp % 1))/1) & 0x0F)+ 0x30);
				UDR = ((temp & 0x0F)+ 0x30);



			}
			else if(compareString(input,def))
			{

				LCDgotoxy(1,0);
				LCDstring("             ");
				enableHeartBeat = 0xff;

			}
			
			
			
			/*
			switch(USARTdata)
			{
				case 'm':
						enableHeartBeat = 0;
						LCDgotoxy(1,0);
						LCDstring("Max beat: ");	
						LCDuint8(maxNoOfBeats);
						LCDchar(' ');
						if(isCharPrinted)
						{
							LCDgotoxy(1,15);
							LCDchar(' ');
						}
						break;
				case 'n':
						enableHeartBeat = 0;
						LCDgotoxy(1,0);
						LCDstring("Min beat: ");	
						LCDuint8(minNoOfBeats);
						LCDchar(' ');
						if(isCharPrinted)
						{
							LCDgotoxy(1,15);
							LCDchar(' ');
						}
						break;
				case 'h':
						
						LCDgotoxy(1,0);
						LCDstring("            ");
						enableHeartBeat = 0xff;
				

			}

			*/
			
		
		}
		
		if(USARTdata != 0)
		{
			if(UCSRA & 0b00100000)
			{
			UDR = USARTdata; // echo back
			USARTdata = 0; // clear the local variable so that no character left
			}
		}
	}

	return 0;
}	

