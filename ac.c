/***************************************************************************************
* FILE NAME: adc.c                                                                   
* AUTHOR: ALI AL HUBAIL																 
* PROVIDE:																			 
* THIS FILE HAS FUNCTIONS TO INISIALIZE THE ANALOG-TO-DIGITAL CONVERTER, TO CHANGE THE FREQUENCY,
* TO ALLOW THE LEFT ADJUSTMENT, TO CHANGE THE PIN, AND TO SELECT THE POWER
****************************************************************************************/

#include <avr\io.h>
#include <inttypes.h>
#include <avr\interrupt.h>

#include "ac.h"


void ACinit(void)
{
	ACSR = 0b00000011;
}


void AC_on(void)
{
	ACSR = ACSR & 0b01111111;	
}

	
void AC_off(void)
{
	ACSR = ACSR | 0b10000000;
}

void AC_useInterrupt(void)
{
	ACSR = ACSR | 0b00001000;
}

void AC_noInterrupt(void)
{
	ACSR = ACSR & 0b11110111;
}

uint8_t AC_isOn(void)
{
	if(ACSR & 0b10000000)	// off
	{
		return 0;
	}
	else
	{
		return 1;
	}
}


