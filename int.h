
#ifndef INT
#define INT
void INT0_on(void);
void INT1_on(void);
void INT2_on(void);
void INT0_off(void);
void INT1_off(void);
void INT2_off(void);

uint8_t INT0_isOn(void);
uint8_t INT1_isOn(void);
uint8_t INT2_isOn(void);

void INT0_fallingEdgeInterrupt(void);
void INT0_risingEdgeInterrupt(void);
void INT1_fallingEdgeInterrupt(void);
void INT1_risingEdgeInterrupt(void);

#endif
