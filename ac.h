
#ifndef AC
#define AC

void ACinit(void);
void AC_on(void);
void AC_off(void);
void AC_useInterrupt(void);
void AC_noInterrupt(void);
uint8_t AC_isOn(void);

#endif
