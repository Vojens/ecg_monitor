#include <avr\io.h>
#include <inttypes.h>
#include <avr\interrupt.h>

#include "tc.h"


void TC0_interrupt_on(void)
{
	TIMSK = TIMSK | 0b00000001;
}

void TC0_interrupt_off(void)
{
	TIMSK = TIMSK & 0b11111110;
}

void TC0_prescaler_selector(uint16_t ps)
{
	auto uint8_t binary_ps;
	switch(ps)
	{
		case 0:
			binary_ps = 0b00000000;
			break;
		case 1:
			binary_ps = 0b00000001;
			break;
		case 8:
			binary_ps = 0b00000010;
			break;
		case 64:
			binary_ps = 0b00000011;
			break;
		case 256:
			binary_ps = 0b00000100;
			break;
		case 1024:
			binary_ps = 0b00000101;
			break;
		case 'f':
			binary_ps = 0b00000110;
			break;

		case 'r':
			binary_ps = 0b00000111;
			break;

		default:
			binary_ps = 0b00000000;
	}

	TCCR0 = TCCR0 | binary_ps;
}

void TC0_set_reg(uint8_t reg)
{
	TCNT0 = reg;
}

uint8_t TC0_get_reg(void)
{
	return TCNT0;
}



void TC1_interrupt_on(void)
{
	TIMSK = TIMSK | 0b00000100;	// Timer/Counter1, Overflow Interrupt Enable
}

void TC1_interrupt_off(void)
{
	TIMSK = TIMSK & 0b11111011;
}

void TC1_prescaler_selector(uint16_t ps)
{
	auto uint8_t binary_ps;
	switch(ps)
	{
		case 0:
			binary_ps = 0b00000000;
			break;
		case 1:
			binary_ps = 0b00000001;
			break;
		case 8:
			binary_ps = 0b00000010;
			break;
		case 64:
			binary_ps = 0b00000011;
			break;
		case 256:
			binary_ps = 0b00000100;
			break;
		case 1024:
			binary_ps = 0b00000101;
			break;
		case 'f':
			binary_ps = 0b00000110;
			break;

		case 'r':
			binary_ps = 0b00000111;
			break;

		default:
			binary_ps = 0b00000000;
	}

	
	TCCR1B = TCCR1B | binary_ps;
}

void TC1_set_reg(uint8_t reg)
{
	TCNT1 = reg;
}

uint16_t TC1_get_reg(void)
{
	return TCNT1;
}
