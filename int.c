#include <avr\io.h>
#include <inttypes.h>
#include <avr\interrupt.h>

#include "int.h"


void INT0_on(void)
{
	GICR = GICR | 0b01000000; // set INT0
}

void INT1_on(void)
{
	GICR = GICR | 0b10000000; // set INT1
}

void INT2_on(void)
{
	GICR = GICR | 0b00100000; // set INT2
}

void INT0_off(void)
{
	GICR = GICR & 0b10111111; //clear INT0
}

void INT1_off(void)
{
	GICR = GICR & 0b01111111; //clear INT0
}

void INT2_off(void)
{
	GICR = GICR & 0b11011111; //clear INT0
}


uint8_t INT0_isOn(void)
{
	if(GICR & 0b01000000)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

uint8_t INT1_isOn(void)
{
	if(GICR & 0b10000000)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

uint8_t INT2_isOn(void)
{
	if(GICR & 0b00100000)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


void INT0_fallingEdgeInterrupt(void)
{
	MCUCR = MCUCR | 0b00000010;	//The falling edge of INT0 generates an interrupt request.
}

void INT0_risingEdgeInterrupt(void)
{
	MCUCR = MCUCR | 0b00000011;	//The risinging edge of INT0 generates an interrupt request.
}

void INT1_fallingEdgeInterrupt(void)
{
	MCUCR = MCUCR | 0b00001000;	//The falling edge of INT1 generates an interrupt request.
}

void INT1_risingEdgeInterrupt(void)
{
	MCUCR = MCUCR | 0b00001100;	//The risinging edge of INT1 generates an interrupt request.
}
